const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
const contractJson = require('./build/contracts.test.json').contracts['redenvelope.sol:RedEnvelopes'];

function eth (a) {
    return web3.utils.toWei(String(a), 'ether');
}

(async function main () {
    const accounts = await web3.eth.getAccounts();
    for (const acc of accounts) {
        const bal = await web3.eth.getBalance(acc);
        console.log('%s: %s', acc, web3.utils.fromWei(bal));
    }


    console.log('deploy %s bytes', contractJson.bin.length);
    const deploy = await (new web3.eth.Contract(JSON.parse(contractJson.abi)))
        .deploy({data: contractJson.bin});
    var gas = await deploy.estimateGas();
    console.log('gas estimated:', gas);
    const contract = await deploy
        .send({from: accounts[0], gas: gas});

    console.log('deployed at', contract.options.address);
    contract.setProvider(provider);

    await contract.methods.authenticate('1111').send({from: accounts[0]});
    await contract.methods.__callback('0x0000000000000000000000000000000000000000000000000000000000000000', accounts[0]).send({from: accounts[0]});

    await contract.methods.authenticate('2222').send({from: accounts[1]});
    await contract.methods.__callback('0x0000000000000000000000000000000000000000000000000000000000000001', accounts[1]).send({from: accounts[0]});

    await contract.methods._forumIdToAddr('1111').call().then(console.log);
    await contract.methods._forumIdToAddr('2222').call().then(console.log);
    await contract.methods.addrToForumId(accounts[0]).call().then(console.log);
    await contract.methods.addrToForumId(accounts[1]).call().then(console.log);

    var gas =  await contract.methods.give(1000, '').estimateGas();
    console.log('give() gas', gas); // 78385
    await contract.methods.give(eth(10), '').send({from: accounts[0], value: eth(20), gas: gas*10}); // underestimated

    await web3.eth.getBalance(accounts[0]).then(console.log);
    await contract.methods.open(0).call({from: accounts[0]}).then(console.log);
    await contract.methods.open(0).send({from: accounts[0]});
    await web3.eth.getBalance(accounts[0]).then(console.log);

    await web3.eth.getBalance(accounts[1]).then(console.log);
    await contract.methods.open(0).call({from: accounts[1]}).then(console.log);
    await contract.methods.open(0).send({from: accounts[1]});
    await web3.eth.getBalance(accounts[1]).then(console.log);

    try {
        await contract.methods.open(0).send({from: accounts[2]});
    } catch (e) {
        console.log('unauth open:', e.message);
    }

    await contract.methods.envelopes(0).call().then(console.log);

    await contract.methods.concat("xxx", "yyy", "z").call().then(console.log);
})().catch(function (e) {
    console.log('Error!');
    console.log(e);
});
