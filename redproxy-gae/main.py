import re
import json
import urllib
import urlparse
from lxml import etree
parser = etree.HTMLParser()

MAX_UID_LENGTH = 20

def app(environ, start_response):
    def error_402 ():
        start_response('402 ', [('content-length', '0')])
        return ['']

    if environ['REQUEST_METHOD'] == 'POST':
        try:
            length= int(environ.get('CONTENT_LENGTH', '0'))
            if (length > MAX_UID_LENGTH):
                return error_402()

            uid = re.match(r'[^\d]*(\d+)', environ['wsgi.input'].read(length)).group(1)
        except:
            return error_402()
    elif environ['REQUEST_METHOD'] == 'GET':
        try:
            uid = urlparse.parse_qs(environ['QUERY_STRING'])['uid'][0]
        except:
            return error_402()
    else:
        return error_402()

    i = 0
    addr = None
    while True:
        try:
            #soup = BeautifulSoup(urllib.urlopen(), 'html.parser')
            #addr = soup.select_one('div#ct div.xld.xlda dl:nth-of-type(1) dd:nth-of-type(2) span ').string
            p = etree.parse(urllib.urlopen('http://8btc.com/home.php?mod=space&do=doing&uid='+uid), parser).xpath('//*[@id="ct"]//div[@class="xld xlda"]/dl[1]/dd[2]/span/text()')
            if len(p):
                addr = p[0]
                break
            else:
                return error_402()
                
        except Exception as e:
            print e
            i += 1
            if i==5:
                start_response('502 internal error', [('content-length', '0')])
                return ['']

    start_response('200 ok', [('content-type', 'application/json')])
    return [json.dumps({'addr': addr})]

