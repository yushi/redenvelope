pragma solidity ^0.4.19;

import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";

contract OraclizeTest is usingOraclize {
   function pay () payable {}

   bytes32 public lastQueryId;
   string public callbackResult;

   function query () payable {
       lastQueryId = oraclize_query("URL", 
            "json(http://redproxy-1.appspot.com/getAddress.php?uid=250950).addr"
       );
   }

   function __callback (bytes32 queryId, string result) {
       //require(msg.sender == oraclize_cbAddress());
       callbackResult = result;
   }
}
