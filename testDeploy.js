const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
const contractJson = require('./build/contracts.json').contracts['redenvelope.sol:RedEnvelopes'];

(async function main () {
    const accounts = await web3.eth.getAccounts();


    const deploy = await (new web3.eth.Contract(JSON.parse(contractJson.abi)))
        .deploy({data: contractJson.bin});
    var gas = await deploy.estimateGas();
    console.log('gas estimated:', gas);
    const contract = await deploy
        .send({from: accounts[0], gas: gas});

    console.log('deployed at', contract.options.address);
    console.log('account 3', accounts[3])
})().catch(function (e) {
    console.log('Error!');
    console.log(e);
});
