const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
const contractJson = require('./build/contracts.json').contracts['redenvelope.sol:RedEnvelopes'];

function eth (a) {
    return web3.utils.toWei(String(a), 'ether');
}

(async function main () {
    const accounts = await web3.eth.getAccounts();

    const contract = new web3.eth.Contract(JSON.parse(contractJson.abi), process.argv[2]);


    await contract.methods.give('berich', eth(1), 'hukairz').send({from: accounts[1], value: eth(10), gas: 999999}).then(console.log);

    await contract.methods._envelopes('berich', 0).call().then(console.log);

    //await web3.eth.getTransactionReceipt('0xbfb0199d8a71634255afc37061ad91bb8f71fe81d28a290a54dee47d29c54cc9').then(console.log);
})().catch(function (e) {
    console.log('Error!');
    console.log(e);
});
