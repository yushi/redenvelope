SOLC = ../bin/solc
COMPILE_TEST_TARGET = build/contracts.test.json
COMPILE_TARGET = build/contracts.json
SOL_SOURCE = redenvelope.sol

compile_test: $(COMPILE_TEST_TARGET)
compile: $(COMPILE_TARGET)

$(COMPILE_TEST_TARGET): $(SOL_SOURCE) 
	$(SOLC) --combined-json bin,abi \
	github.com/oraclize/ethereum-api/oraclizeAPI.sol=lib/oraclizeAPI.sol \
	$^ > $@

$(COMPILE_TARGET): $(SOL_SOURCE) 
	$(SOLC) --optimize --combined-json bin,abi \
	github.com/oraclize/ethereum-api/oraclizeAPI.sol=lib/oraclizeAPI.sol \
	$^ > $@

test: compile_test
	node test.js

abi: compile
	@cat build/contracts.json | jq '.contracts' | jq '.["redenvelope.sol:RedEnvelopes"]' | jq '.abi'

bin: compile
	@cat build/contracts.json | jq '.contracts' | jq '.["redenvelope.sol:RedEnvelopes"]' | jq -r '.bin'

code:
	@cat lib/oraclizeAPI.sol
	@cat $(SOL_SOURCE) | sed '/^import/d'
