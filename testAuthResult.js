const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
const contractJson = require('./build/contracts.json').contracts['redenvelope.sol:RedEnvelopes'];

function eth (a) {
    return web3.utils.toWei(String(a), 'ether');
}

(async function main () {
    const accounts = await web3.eth.getAccounts();

    const contract = new web3.eth.Contract(JSON.parse(contractJson.abi), process.argv[2]);


    var a = await contract.methods._forumIdToAddr('250950').call();
    console.log('addr:', a);
    await contract.methods.addrToForumId(a).call().then(console.log);
})().catch(function (e) {
    console.log('Error!');
    console.log(e);
});
